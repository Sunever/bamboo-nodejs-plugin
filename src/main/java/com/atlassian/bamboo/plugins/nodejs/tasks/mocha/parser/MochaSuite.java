package com.atlassian.bamboo.plugins.nodejs.tasks.mocha.parser;

import com.google.common.base.Objects;

import java.util.List;

/**
 * A representation of Mocha test suite results. Objects of this class are automatically created by deserializing Mocha
 * test results file (JSON format), which is a result of using Mocha with `mocha-bamboo-reporter`.
 */
public class MochaSuite
{
    private MochaSuiteStats stats;
    private List<MochaSuiteTest> skipped;
    private List<MochaSuiteTest> failures;
    private List<MochaSuiteTest> passes;

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("tests", stats.getTests())
                .add("passed", passes.size())
                .add("failed", failures.size())
                .add("skipped", skipped.size())
                .toString();
    }

    public MochaSuiteStats getStats()
    {
        return stats;
    }

    public List<MochaSuiteTest> getSkipped()
    {
        return skipped;
    }

    public List<MochaSuiteTest> getFailures()
    {
        return failures;
    }

    public List<MochaSuiteTest> getPasses()
    {
        return passes;
    }
}
