package com.atlassian.bamboo.plugins.nodejs.tasks.mocha.parser;

import com.google.common.base.Objects;

import java.util.Date;

/**
 * A representation of Mocha test suite statistics. Objects of this class are automatically created by deserializing
 * Mocha test results file (JSON format), which is a result of using Mocha with `mocha-bamboo-reporter`.
 */
public class MochaSuiteStats
{
    private int suites;
    private int tests;
    private int passes;
    private int pending;
    private int failures;
    private Date start;
    private Date end;
    private int duration;

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("suites", suites)
                .add("tests", tests)
                .add("passes", passes)
                .add("pending", pending)
                .add("failures", failures)
                .add("start", start)
                .add("end", end)
                .add("duration", duration)
                .toString();
    }

    public int getSuites()
    {
        return suites;
    }

    public int getTests()
    {
        return tests;
    }

    public int getPasses()
    {
        return passes;
    }

    public int getPending()
    {
        return pending;
    }

    public int getFailures()
    {
        return failures;
    }

    public Date getStart()
    {
        return start;
    }

    public Date getEnd()
    {
        return end;
    }

    public int getDuration()
    {
        return duration;
    }
}