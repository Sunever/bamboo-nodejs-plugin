package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import com.atlassian.bamboo.build.CustomBuildProcessor;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.agent.ExecutableBuildAgent;
import com.atlassian.bamboo.v2.build.agent.capability.AgentContext;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import static com.google.common.base.Preconditions.checkState;

/**
 * A custom build processor that cleans up the temporary cache directory used by npm tasks.
 */
public class NpmCacheCleanupProcessor implements CustomBuildProcessor
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(NpmIsolatedCacheHelper.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    private BuildContext buildContext;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    private AgentContext agentContext;

    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void init(@NotNull BuildContext buildContext)
    {
        this.buildContext = buildContext;
    }

    @NotNull
    @Override
    public BuildContext call() throws Exception
    {
        final ExecutableBuildAgent buildAgent = agentContext.getBuildAgent();
        checkState(buildAgent != null, "Agent should be always set within build pipeline");

        NpmIsolatedCacheHelper.deleteTemporaryCacheDirectory(buildAgent.getId());

        return buildContext;
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    public void setAgentContext(AgentContext agentContext)
    {
        this.agentContext = agentContext;
    }
}