package com.atlassian.bamboo.plugins.nodejs.tasks.nodeunit;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.mocha.parser.MochaParserTaskType;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.utils.process.ExternalProcess;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class NodeunitTaskType implements TaskType
{
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final ProcessService processService;
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final CapabilityContext capabilityContext;
    private final I18nResolver i18nResolver;
    private final TestCollationService testCollationService;

    // ---------------------------------------------------------------------------------------------------- Constructors
    public NodeunitTaskType(final ProcessService processService,
                            final EnvironmentVariableAccessor environmentVariableAccessor,
                            final CapabilityContext capabilityContext,
                            final I18nResolver i18nResolver,
                            final TestCollationService testCollationService)
    {
        this.processService = processService;
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.capabilityContext = capabilityContext;
        this.i18nResolver = i18nResolver;
        this.testCollationService = testCollationService;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    public TaskResult execute(@NotNull TaskContext taskContext) throws TaskException
    {
        try
        {
            final BuildLogger buildLogger = taskContext.getBuildLogger();
            final ConfigurationMap configurationMap = taskContext.getConfigurationMap();

            final String nodeRuntime = configurationMap.get(NodeunitConfigurator.NODE_RUNTIME);
            final String nodePath = capabilityContext.getCapabilityValue(NodeCapabilityDefaultsHelper.NODE_CAPABILITY_PREFIX + "." + nodeRuntime);
            Preconditions.checkState(StringUtils.isNotBlank(nodePath), i18nResolver.getText("node.runtime.error.undefinedPath"));

            final String nodeunitPath = configurationMap.get(NodeunitConfigurator.NODEUNIT_RUNTIME);
            Preconditions.checkState(StringUtils.isNotBlank(nodeunitPath), i18nResolver.getText("nodeunit.runtime.error.undefinedPath"));

            final String testFiles = configurationMap.get(NodeunitConfigurator.TEST_FILES);
            final String testResultsDirectory = configurationMap.get(NodeunitConfigurator.TEST_RESULTS_DIRECTORY);

            final Map<String, String> extraEnvironmentVariables = environmentVariableAccessor.splitEnvironmentAssignments(configurationMap.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES), false);

            final ImmutableList.Builder<String> commandListBuilder = ImmutableList.<String>builder().add(nodePath, nodeunitPath);
            commandListBuilder.add(
                    "--reporter", "junit",
                    "--output", testResultsDirectory);

            commandListBuilder.addAll(CommandlineStringUtils.tokeniseCommandline(testFiles));

            // additional command line arguments
            final String arguments = configurationMap.get(NodeunitConfigurator.ARGUMENTS);
            if (StringUtils.isNotBlank(arguments))
            {
                buildLogger.addBuildLogEntry("Using additional command line arguments: " + arguments);
                commandListBuilder.addAll(CommandlineStringUtils.tokeniseCommandline(arguments));
            }

            final ExternalProcess executeTestsProcess = processService.executeExternalProcess(taskContext, new ExternalProcessBuilder()
                    .command(commandListBuilder.build())
                    .env(extraEnvironmentVariables)
                    .path(FilenameUtils.getFullPath(nodePath))
                    .workingDirectory(taskContext.getWorkingDirectory()));

            if (executeTestsProcess.getHandler().isComplete())
            {
                final TaskResultBuilder taskResultBuilder = TaskResultBuilder
                        .newBuilder(taskContext)
                        .checkReturnCode(executeTestsProcess);

                final boolean parseTestResults = configurationMap.getAsBoolean(NodeunitConfigurator.PARSE_TEST_RESULTS);
                if (parseTestResults)
                {
                    final String testFilePattern = MochaParserTaskType.calculateEffectiveFilePattern(taskContext, testResultsDirectory);
                    testCollationService.collateTestResults(taskContext, testFilePattern);
                    taskResultBuilder.checkTestFailures();
                }

                return taskResultBuilder.build();
            }

            throw new TaskException("Failed to execute command, external process not completed");
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }

    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
