package com.atlassian.bamboo.plugins.nodejs.tasks.node;

import com.atlassian.bamboo.v2.build.agent.capability.AbstractFileCapabilityDefaultsHelper;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import org.apache.commons.lang3.SystemUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class NodeCapabilityDefaultsHelper extends AbstractFileCapabilityDefaultsHelper
{
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String NODE_EXECUTABLE_NAME = SystemUtils.IS_OS_WINDOWS ? "node.exe" : "node";
    public static final String NPM_EXECUTABLE_NAME = SystemUtils.IS_OS_WINDOWS ? "npm.cmd" : "npm";

    public static final String NODE_CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + ".node";
    private static final String DEFAULT_NODE_CAPABILITY = NODE_CAPABILITY_PREFIX + ".Node.js";

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    protected String getExecutableName()
    {
        return NODE_EXECUTABLE_NAME;
    }

    @NotNull
    @Override
    protected String getCapabilityKey()
    {
        return DEFAULT_NODE_CAPABILITY;
    }

    @Nullable
    @Override
    protected String getEnvHome()
    {
        return System.getenv("NODE_HOME");
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
