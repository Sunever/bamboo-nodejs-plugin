package com.atlassian.bamboo.plugins.nodejs.tasks.mocha.parser;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Set;

public class MochaParserConfigurator extends AbstractNodeTaskConfigurator implements TaskTestResultsSupport
{
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String PATTERN = "testPattern";

    protected static final Set<String> FIELDS_TO_COPY = ImmutableSet.<String>builder()
            .addAll(AbstractNodeTaskConfigurator.FIELDS_TO_COPY)
            .add(PATTERN)
            .add(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE)
            .build();
    protected static final Map<String, Object> DEFAULT_FIELD_VALUES = ImmutableMap.<String, Object>builder()
            .putAll(AbstractNodeTaskConfigurator.DEFAULT_FIELD_VALUES)
            .put(PATTERN, "mocha.json")
            .build();

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);
        if (StringUtils.isBlank(params.getString(PATTERN)))
        {
            errorCollection.addError(PATTERN, i18nResolver.getText("mocha.parser.testPattern.error.empty"));
        }
    }

    @Override
    public boolean taskProducesTestResults(@NotNull TaskDefinition taskDefinition)
    {
        return true;
    }

    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    @NotNull
    @Override
    public Set<String> getFieldsToCopy()
    {
        return FIELDS_TO_COPY;
    }

    @NotNull
    @Override
    public Map<String, Object> getDefaultFieldValues()
    {
        return DEFAULT_FIELD_VALUES;
    }
}
