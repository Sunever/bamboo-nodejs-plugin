package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import org.junit.Test;

import java.io.File;

import static com.atlassian.bamboo.testutils.matchers.FileMatcher.exists;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class NpmIsolatedCacheHelperTest
{
    @Test
    public void testTemporaryCacheDirectory() throws Exception
    {
        final long agentId = 1000L;
        final long differentAgentId = 2000L;

        final File cacheDirectory = NpmIsolatedCacheHelper.initializeTemporaryCacheDirectory(agentId);
        assertThat("Should have created cache directory", cacheDirectory, exists());

        final File testFile = new File(cacheDirectory, "testFile.txt");
        testFile.createNewFile();
        assertThat("Should have created a file inside cache directory", testFile, exists());

        final File sameCacheDirectory = NpmIsolatedCacheHelper.initializeTemporaryCacheDirectory(agentId);
        assertThat("Another call to 'initialize cache directory' method should return the same directory",
                sameCacheDirectory, is(cacheDirectory));
        assertThat("Cache directory should exist after second call to 'initialize cache directory' method",
                cacheDirectory, exists());
        assertThat("Every file inside cache directory should have been removed after second call to 'initialize cache directory' method",
                testFile, not(exists()));

        final File differentCacheDirectory = NpmIsolatedCacheHelper.initializeTemporaryCacheDirectory(differentAgentId);
        assertThat("Should have created another cache directory", differentCacheDirectory, exists());
        assertThat("Call to 'initialize cache directory' method with different arguments should return different directory",
                differentCacheDirectory, is(not(cacheDirectory)));

        NpmIsolatedCacheHelper.deleteTemporaryCacheDirectory(agentId);
        assertThat("Should have deleted cache directory", cacheDirectory, not(exists()));
        assertThat("Shouldn't have deleted a different cache directory", differentCacheDirectory, exists());

        NpmIsolatedCacheHelper.deleteTemporaryCacheDirectory(differentAgentId);
        assertThat("Should have deleted the second cache directory", differentCacheDirectory, not(exists()));
    }
}