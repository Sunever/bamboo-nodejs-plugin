package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.pageobjects.pages.plan.result.JobResultViewLogs;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plugins.nodejs.tasks.npm.NpmConfigurator;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.NpmTaskComponent;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.Map;
import java.util.Set;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isEmptyOrNullString;

/**
 * Tests for "npm" Bamboo task. Only contains non-trivial tests, as basic npm component is used in almost every other
 * test.
 */
public class NpmTaskTest extends AbstractNodeTaskTest
{
    @Test
    public void testTaskWithIsolatedCache() throws Exception
    {
        product.gotoLoginPage().loginAsSysAdmin();

        final TestBuildDetails plan = createAndSetupPlan();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, plan.getDefaultJob());

        // create two npm tasks using isolated cache
        final Map<String, String> npmInstallGrunt = ImmutableMap.of(
                NpmConfigurator.COMMAND, "install grunt",
                NpmConfigurator.ISOLATED_CACHE, Boolean.TRUE.toString());

        final Map<String, String> npmInstallGulp = ImmutableMap.of(
                NpmConfigurator.COMMAND, "install gulp",
                NpmConfigurator.ISOLATED_CACHE, Boolean.TRUE.toString());

        taskConfigurationPage.addNewTask(NpmTaskComponent.TASK_NAME, NpmTaskComponent.class, "npm install grunt", npmInstallGrunt);
        taskConfigurationPage.addNewTask(NpmTaskComponent.TASK_NAME, NpmTaskComponent.class, "npm install gulp", npmInstallGulp);

        // execute and assert execution was successful
        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 1);

        // analyze logs - assert only one cache directory was used
        final PlanKey jobKey = plan.getDefaultJob().getKey();
        final PlanResultKey resultKey = PlanKeys.getPlanResultKey(jobKey, 1);
        final JobResultViewLogs logsPage = product.visit(JobResultViewLogs.class, resultKey);

        final String cacheUsageLogIndicator = getText("npm.isolatedCache.usage", StringUtils.EMPTY);
        assertThat("Should have found an i18n property", cacheUsageLogIndicator, not(isEmptyOrNullString()));
        assert cacheUsageLogIndicator != null;

        final Iterable<String> cacheUsagesInLog = Iterables.filter(logsPage.getLog(), Predicates.containsPattern(cacheUsageLogIndicator));

        final Set<String> usedCacheDirectories = Sets.newHashSet();
        for (String cacheUsage : cacheUsagesInLog)
        {
            // we extract only the cache directory - omit log timestamp and the indicator
            usedCacheDirectories.add(cacheUsage.substring(cacheUsage.indexOf(cacheUsageLogIndicator) + cacheUsageLogIndicator.length()));
        }

        assertThat("Should have used only one isolated cache directory for subsequent npm tasks", usedCacheDirectories, hasSize(1));
    }


}