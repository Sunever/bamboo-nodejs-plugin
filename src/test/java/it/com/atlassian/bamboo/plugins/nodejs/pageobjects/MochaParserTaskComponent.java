package it.com.atlassian.bamboo.plugins.nodejs.pageobjects;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.bamboo.plugins.nodejs.tasks.mocha.parser.MochaParserConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import org.apache.log4j.Logger;

import java.util.Map;

public class MochaParserTaskComponent extends ComponentWithAdvancedOptions implements TaskComponent
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(MochaParserTaskComponent.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String TASK_NAME = "Mocha Test Parser";

    // ------------------------------------------------------------------------------------------------- Type Properties
    @ElementBy(name = MochaParserConfigurator.PATTERN)
    private TextElement patternField;

    @ElementBy(name = TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE)
    private CheckboxElement pickupOutdatedResultsCheckbox;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void updateTaskDetails(Map<String, String> config)
    {
        this.withAdvancedOptions();

        if (config.containsKey(MochaParserConfigurator.PATTERN))
        {
            patternField.setText(config.get(MochaParserConfigurator.PATTERN));
        }
        if (config.containsKey(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY))
        {
            workingSubDirectoryField.setText(config.get(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE))
        {
            final Boolean pickupOutdatedResults = Boolean.parseBoolean(config.get(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE));
            if (pickupOutdatedResults)
            {
                pickupOutdatedResultsCheckbox.check();
            }
            else
            {
                pickupOutdatedResultsCheckbox.uncheck();
            }
        }
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}